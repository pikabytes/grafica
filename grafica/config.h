#ifndef CONFIG_H
#define CONFIG_H

#include <GL/glut.h>
#include "functions.h"


using namespace std;

#define height 600
#define width 600


void reshape(int w, int h) {
    if (w==0||h==0) return;
    glViewport(0,0,w,h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluOrtho2D(0,w,0,h);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
}

void display(){
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,1,0); glLineWidth(3);
    polygon(height/2,width/2);

    draw_pixel(100,100);


    glutSwapBuffers();
}





void initialize(){
    glutInitDisplayMode (GLUT_RGBA|GLUT_DOUBLE);
    glutInitWindowSize (height,width);
    //glutInitWindowPosition (200,500);
    glutCreateWindow ("Ventana OpenGL");
    glutDisplayFunc (display);
    glutReshapeFunc (reshape);
    glClearColor(0.f,0.f,0.f,1.f);
}



#endif // CONFIG_H
