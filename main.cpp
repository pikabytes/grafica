#include <GL/glut.h>
#include "config.h"
#include "functions.h"
using namespace std;



int main (int argc, char **argv) {
    glutInit (&argc, argv);
    initialize();
    glutMainLoop();
    return 0;
}