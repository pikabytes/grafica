#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <GL/glut.h>
#include <math.h>
#include <vector>
#include <utility>      // std::pair, std::make_pair
#include <iostream>
#define pi 3.14159


using namespace std;


void line(int x1,int y1,int x2,int y2){
	glBegin(GL_LINES);
	glVertex2f(x1,y1);
	glVertex2f(x2,y2);
	glEnd();
}


void polygon(int x_c, int y_c){
        int radio, sides;
        cout<<"ingrese radio: ";cin>>radio;
        cout<<"ingrese lados: ";cin>>sides;
	int centerX = x_c/2;
	int centerY = y_c/2;
	float angle = 2*pi/sides;
	vector<pair<int,int> > edges;
	for (int i = 0; i < sides; ++i){
		int x = x_c + radio*cos(angle*i);
		int y = y_c + radio*sin(angle*i);
		edges.push_back(make_pair(x,y));
        }

	int x_past= edges[0].first, y_past =edges[0].second;

	glBegin(GL_LINES);
		for (int i = 1; i < edges.size(); ++i){
			glVertex2f(x_past,y_past);
			glVertex2f(edges[i].first, edges[i].second);
			x_past= edges[i].first;
			y_past= edges[i].second;
		}
		glVertex2f(x_past,y_past);
		glVertex2f(edges[0].first, edges[0].second);
	glEnd();	

}

void draw_pixel(int x, int y){
	glColor3f(0.0,1.0,1.0);
    glPointSize(3.0);
    glBegin(GL_POINTS);
    glVertex2i(x,y);
    glEnd();
}



void hyperbola()  
{ 
	double x,y;
	glColor3f(1.0, 1.0, 0.0);		
	glBegin( GL_POINTS );		
	for (x = -1; x <= 1; x += .001) 
	{ y =  4* x -  4* x * x ;     
	glVertex3f(x, y, 0.0);      
	}
	glEnd();				
}


void elipse()
{
	float xc=300, yc=300,  rx=200, ry=100;
	float rxSq = rx * rx;
	float rySq = ry * ry;
	float x = 0, y = ry, p;
	float px = 0, py = 2 * rxSq * y;
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f( 1,1,0);
	
	glBegin(GL_POINTS);
		glVertex2d(xc+x,yc-y);
		glVertex2d(xc-x,yc-y);
		glVertex2d(xc-x,yc+y);
	glEnd();
	
	p = rySq - (rxSq * ry) + (0.25 * rxSq);
	while (px < py){
		x++;
		px = px + 2 * rySq;
		if (p < 0)
			p = p + rySq + px;
		else
		{
			y--;
			py = py - 2 * rxSq;
			p = p + rySq + px - py;
		}
		glBegin(GL_POINTS);
		
		glVertex2d(xc+x,yc+y);
		glVertex2d(xc+x,yc-y);
		glVertex2d(xc-x,yc-y);
		glVertex2d(xc-x,yc+y);
		glEnd();
	}
	
	p = rySq*(x+0.5)*(x+0.5) + rxSq*(y-1)*(y-1) - rxSq*rySq;
	while (y > 0){
		y--;
		py = py - 2 * rxSq;
		if (p > 0)
			p = p + rxSq - py;
		else
		{
			x++;
			px = px + 2 * rySq;
			p = p + rxSq - py + px;
		}
		glBegin(GL_POINTS);
		
		glVertex2d(xc+x,yc+y);
		glVertex2d(xc+x,yc-y);
		glVertex2d(xc-x,yc-y);
		glVertex2d(xc-x,yc+y);
		glEnd();
		glFlush();		
	}
}


void drawPolygon(int width, int hight){
    vector<unsigned char> pixels(1*1*4);

    for(int i=0; i<width; ++i){
        for(int j=0; i<hight; ++i){

        }

    }
}



#endif // FUNCTIONS_H





















