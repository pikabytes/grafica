OBJS = main.cpp 

CC = g++

COMPILER_FLAGS = 

LINKER_FLAGS = -lGL -lGLU -lglut

OBJ_NAME = out

all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
